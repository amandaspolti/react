import React from 'react';
import Enzyme from 'enzyme';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';

import Footer from '../components/template/Footer';


Enzyme.configure({ adapter: new Adapter() });
const wrapper = shallow(<Footer />);
describe('<Footer />', () => {
  it('renders without exploding', () => {
      const wrapper = mount(<Footer />);
      expect(wrapper).to.have.length(1);
  });
  it('classMame should be equal to footer ', () => {
    expect(wrapper.hasClass('footer')).to.equal(true);
  });
  it('Footer should contain a <p> element ', () => {
    expect(wrapper.contains(<p className="lead text-muted">{}</p>)).to.equal(false);

  });

})

