import React from 'react';
import Enzyme from 'enzyme';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';

import Logo from '../components/template/Logo';


Enzyme.configure({ adapter: new Adapter() });

describe('<Logo/>', function () {
    it('renders without exploding', () => {
      const wrapper = shallow(<Logo />);
      expect(wrapper).to.have.length(1);
    });
    it('should have an image to display the logo', function () {
      const wrapper = shallow(<Logo/>);
      expect(wrapper.find('img')).to.have.length(1);
    });
    it('at least one of the nodes in the current wrapper matched the provided selector', function () {
        const wrapper = shallow(<Logo/>);
        expect(wrapper.find('.logo').some('.logo')).to.equal(true);
      });
    
  });




