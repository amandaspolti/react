import React from 'react';
import Enzyme from 'enzyme';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';

import Header from '../components/template/Header';

Enzyme.configure({ adapter: new Adapter() });

describe('<Header />', () => {
  it('renders without exploding', () => {
    const wrapper = mount(<Header />);
      expect(wrapper).to.have.length(1);
    });
  it('className should be equal to ', () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.contains(<h1 className="mt-3" />)).to.equal(false);
  });
  it('Header should contain a <p> element', () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.contains(<p className="lead text-muted">{}</p>)).to.equal(true);

  });

})

