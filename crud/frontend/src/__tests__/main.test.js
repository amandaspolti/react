import React from 'react';
import Enzyme from 'enzyme';
import { shallow, mount, render } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';


import Main from '../components/template/Main';


Enzyme.configure({ adapter: new Adapter() });

describe('Main component testing', function() {
    it('renders without exploding', () => {
      const wrapper = mount(<Main />);
        expect(wrapper).to.have.length(1);
      });

    it('renders three div `.teste`s', () => {
      const wrapper = render(<Main />);
       expect(wrapper.find('.teste')).to.have.length(2);
      });
  });






